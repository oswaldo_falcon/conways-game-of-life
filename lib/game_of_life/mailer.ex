defmodule GameOfLife.Mailer do
  @moduledoc """
  Mailer
  """
  use Swoosh.Mailer, otp_app: :game_of_life
end
